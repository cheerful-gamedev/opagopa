﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using FTRuntime;
using FTRuntime.Yields;

public class VolumeController : MonoBehaviour {

	public bool mute;

	public AudioSource[] audios;
	public SwfClipAsset[] assets;
	public SwfClip clip;
	public SwfClipController controller;

	//public SWF

	void Start()
	{
		if(!PlayerPrefs.HasKey ("Mute"))
			PlayerPrefs.SetString ("Mute", "true");
		mute = bool.Parse(PlayerPrefs.GetString ("Mute"));

		RefreshVolume ();
	}

	public void SwitchMute()
	{
		mute = !mute;
		PlayerPrefs.SetString ("Mute", mute.ToString());

		RefreshVolume ();
	}

	void RefreshVolume()
	{
		int vol = System.Convert.ToInt32 (mute);
		clip.clip = assets[vol];
		controller.Play (true);

		foreach (AudioSource aud in audios)
		{
			aud.volume = 1 - vol;
		}
	}
}
