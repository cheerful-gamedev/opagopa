﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using FTRuntime;
using FTRuntime.Yields;

public class GameManager : MonoBehaviour {

//	Float
	public float 
		distanceBetween = 5, // Дистанция между ботанами
		spawnHeight = -5, // Высота спавна ботанов
		minspawnWidth, // Ширина спавна от
		maxspawnWidth; // до

//	Int
	public int
		curNoob = 2; // Начинаем спавн со второго (первого в массиве)

//	List
	public List<GameObject> noobs; // Список наших ботаников

//	PUBLIC
	public SwfClip Sun_SwfClip, // То где мы меняем анимацию
	Bulk_SwfClip;
	public SwfClipController Sun_SwfClipController, // То где мы запускаем анимацию
	Bulk_SwfClipController, // Бульк
	Semka_SwfClipController, // Семешка
	Shop_SwfClipController; // Магазик
	public SwfClipAsset[] Sun_clips, // Анимации, которые мы меняем
	Bulk_clips;

	public GameObject prazdnik; // Объект с шарами и конфети
	public AudioSource audioPrazdnik; // Звук праздника, уииии:3

// PRIVATE
	private Vector2 spawnPos;

	public void Start()
	{
		// Обнуление под всех Гоп
		prazdnik.active = false;
		Bulk_SwfClip.clip = Bulk_clips [0];
		// Фичи для особых Гоп
		if(PlayerPrefs.GetInt ("ChoicedGopa") == 5)
		{
			Bulk_SwfClip.clip = Bulk_clips [1];
		}
		else if(PlayerPrefs.GetInt ("ChoicedGopa") == 6)
		{
			// Убрать попросили, а то лагает тип PrazdnikPlay ();
		}

		Bulk_SwfClip.currentFrame = Bulk_SwfClip.frameCount; // Заканчиваем анимашку, иначе терминатор очень долго тонет((
		SpawnNoob (); // Заранее спавним второго ботана
		SpawnNoob (); // Заранее спавним и третьего ботана
		StartCoroutine (noobs [0].GetComponent<Noob> ().Swiming (3.2f)); // Заставляем первого ботана выплыть под гопу
		StartCoroutine (noobs [1].GetComponent<Noob> ().Swiming (3.2f)); // Заставляем второго ботана выплыть под гопу
	}

	void Update()
	{
		// Отправляем всем ботанам команту грести
		if(Input.touchCount > 0 || Input.GetMouseButton (0))
		{
			Invoke("SpeedUp", 0);
			Invoke("SpeedDown", 0.3f);
		}
	}

	void SpeedUp() { Manager.speed++; }
	void SpeedDown() { Manager.speed--; }

	public IEnumerator Rake()
	{
		Manager.speed++;
		yield return new WaitForSeconds(.3f);
		Manager.speed--;
	}

	// Спавнер ботанов
	public void SpawnNoob()
	{
		int last_noob = curNoob - 1;
		if(last_noob < 0)
			last_noob = noobs.Count - 1;

		// Расчитываем дистанчию между нубами, если предыдущий нуб не далеко ушел
		float X_pos = 1000; // Позиция для спавна ботана
		Vector2 lastNoob_pos = new Vector2(noobs [last_noob].transform.position.x, noobs [last_noob].transform.position.y); // Позишн ласт ботан

		// Пока не будет преодалено ограничение минимальной дистанции между ботанами
		X_pos = lastNoob_pos.x + Random.Range (minspawnWidth, maxspawnWidth); // Почти рандомные манипуляции
		/*if(X_pos <= 5) // Подстраховочка!
		{
			X_pos = Random.Range (6, 12); // Почти рандомные манипуляции
		}*/
		spawnPos = new Vector2 (X_pos, spawnHeight); // Задаем позицию спавна

		GameObject noob = noobs [curNoob]; // Текущий ботан

		int rand = Random.Range (0, 10);
		if(rand == 3)
		{
			noob.GetComponent<SwfClipController> ().rateScale = 3;
		}
		else
		{
			noob.GetComponent<SwfClipController> ().rateScale = 1;
		}

		noob.transform.position = new Vector2 (transform.position.x, -5);
		noob.transform.position = spawnPos; // Переносим ботана в точку спавна
		StartCoroutine(noob.GetComponent<Noob>().Swiming(3.2f));

		curNoob++;
		if(curNoob >= noobs.Count)
			curNoob = 0;
	}

	// Мы чет тронули? Фууу!
	void OnTriggerEnter2D (Collider2D col)
	{
		GameObject tuchObj = col.gameObject; // Присваиваем объект к объекту для удобного объекто-ориентированного программирования

		if(tuchObj.tag == "Noob" || tuchObj.tag == "GoldNoob") // Проверка на единичность касания
		{
			SpawnNoob ();
		}
	}

	// Смена анимашек у солнца
	public void ChangeSunState()
	{
		if(!Sun_SwfClipController.isPlaying)
		{
			switch (Manager.sunState)
			{
			// Просто покачивание или ожидание
				case "idle":
					Sun_SwfClip.clip = Sun_clips [1];
					Sun_SwfClipController.playMode = SwfClipController.PlayModes.Backward;
					Sun_SwfClipController.Play (true);
					break;
			// Удивление от прыжка Гопы
				case "surprise":
					Sun_SwfClip.clip = Sun_clips [0];
					Sun_SwfClipController.playMode = SwfClipController.PlayModes.Forward;
					Sun_SwfClipController.Play (true);
					break;
			// Радость от смерти Гопы (ценичное солнце)
				case "happy":
					Sun_SwfClip.clip = Sun_clips [1];
					Sun_SwfClipController.playMode = SwfClipController.PlayModes.Forward;
					Sun_SwfClipController.Play (true);
					break;
			}
		}
		else if(Manager.gameOver)
		{
			switch (Manager.sunState)
			{
				// Радость от смерти Гопы (ценичное солнце)
				case "happy":
					Sun_SwfClip.clip = Sun_clips [1];
					Sun_SwfClipController.playMode = SwfClipController.PlayModes.Forward;
					Sun_SwfClipController.Play (true);
					break;
			}
		}
	}

	public void PrazdnikPlay()
	{
		prazdnik.active = false;
		prazdnik.active = true;
		prazdnik.GetComponent<SwfClipController> ().Play (true);

		if(Manager.PrazdnicAudio == true)
		{
			audioPrazdnik.Play ();
			Manager.PrazdnicAudio = false;
		}
	}

	public Transform GetNextNoob()
	{
		// List<GameObject> filteredNoobs = noobs.Find(noob => noob.transform.position.x > 0);
		GameObject nextNoob = noobs.Find(noob => noob.transform.position.x > 0.2f);
		nextNoob.transform.localScale = new Vector3(-1,1,1);
		return nextNoob.transform;//filteredNoobs[curNoob].transform;
	}

}

public class Manager
{
	public static float 
	speed = 3, // Текущая скорость игры
	maxSpeed = 30; // Максимальная скорость игры

	public static string CurNoobName = "", // Имя ботана, что мы трогали недавно (Чтобы не спавнилось кучу ботанов подряд из-за залипания)
	sunState, // Текущее состояние солнца
	adsReward = "Die"; // Награда за просмотр: "Gopa 4" - получаем гопу 4 по завершению, "Die" - ничего не получаем, "Semki" + 2 семки

	public static bool gameOver, // Проиграли ли мы
	showAds, // Показываем ли рекламку
	shopShow, // Показываем магаз
	PrazdnicAudio = true; // Проигрывать ли аудио

	public static int ShopGopaNum; // Номер выбранного Гопы в магазе
}
