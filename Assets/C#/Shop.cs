﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using UnityEngine.Advertisements; // Рекламка

public class Shop : MonoBehaviour {

//	Text
	public Text
		semki, // Кол-во семок у нас в кормане
		price, // Стоимость гопы в семках
		k_semok, // коэф семок текущего Гопы
		k_health, // кол-во жизни текущего Гопы
		buy_text, // Текст Купить/Выбрать
		error; // Текст ошибки в предупреждающем окне

//	Image
	public Image Semki_img, // Картинка семечки, если покупается Гопа за семки
	video_img; // Картинка видео, если покупается Гопа за видео

//	PUBLIC
	public SpriteRenderer MainGopa; // Главный Гопа, тот что прыгает
	public ScrollRect Scroll; // Скроллер
	public GameObject[] Panels = new GameObject[2]; // Паннели с переубеждением при покупке
	public GameObject price_gm; // Gm содержащий картинки и цену за Гопу

	public GameManager manager;

//	PRIVATE
	private RectTransform scrollTransform;
	private RectTransform Content; // Поле с Гопами
	private bool refreshVariables; // Обновляем ли мы текста и прочую дичь

//	MyClass[]
	public GopaHaracteristics[] gops; // Массив Гоп характеристик
	[System.Serializable]
	public class GopaHaracteristics
	{
		public Sprite sprite; // Картинка Гопы
		public bool video, // false - за семки,  true - за видео
		choice, // false - не выбран (и наоборот)
		buyed; // Куплен

		public int price, // Цена
		k_semki = 1, // Коэффициент семечек
		k_health = 1; // Кол-во жизней жизней
	}

	void Start()
	{
		StartCoroutine (TimeFromURL ());
		/*System.DateTime Date = System.DateTime.Now;
		string DateAndMonth = Date.Day + "." + Date.Month;

		if(DateAndMonth == "26.11" || DateAndMonth == "01.12" || DateAndMonth == "1.12")
		{
			StartCoroutine (TimeFromURL ());
		}*/
		//gops [PlayerPrefs.GetInt ("ChoicedGopa")].choice = true;
		ChangeVariablesFromPrefs ();
		Content = Scroll.content.GetComponent<RectTransform> ();
		scrollTransform = Scroll.GetComponent<RectTransform>();
	}

	void Update()
	{
		if(Input.touchCount > 0 || Input.GetMouseButton (0) || Mathf.Abs(Scroll.velocity.x) >= 50)
		{
			// Поиск ближайщего Гопы
			int GopaNum = 0;
			float mindist = 10000;
			for(int i = 0; i < Scroll.content.childCount; ++i)
			{
				float dis = Vector2.Distance (Vector2.zero, Scroll.content.GetChild (i).transform.position);
				if(dis < mindist)
				{
					mindist = dis;
					GopaNum = i;
				}
			}
			Manager.ShopGopaNum = GopaNum;
			refreshVariables = true;
		}
		else if(Input.touchCount == 0 && !Input.GetMouseButton (0))
		{
			if(Mathf.Abs(Scroll.velocity.x) < 50)
			{
				float normalizePosition = (Content.sizeDelta.x+250f); // Расчитываем длину контекта + размер на одного Гопу
				normalizePosition /= (float)Scroll.content.transform.childCount; // Делим на кол-во Гоп (чтоб найти позицию на одного Гопу)
				normalizePosition *= Manager.ShopGopaNum; // Умножаем на номер нужного Гопоря
				normalizePosition /= Content.sizeDelta.x; // И делим все на офигеть какое число, чтобы расчеты были такими ну вот такими 0.001;
				normalizePosition = Mathf.Clamp01 (normalizePosition); // Сложная дичь, те не понять, но она тип стабилизирует значение от 0 до 1

				float startPos = Scroll.horizontalNormalizedPosition; // Присваем начальную позицию для сглаживания
				Scroll.horizontalNormalizedPosition = Mathf.Lerp(startPos, normalizePosition, 0.2f); // Присваеваем умной ф-ции Unity по нормализации контента со сглаживанием

				if(refreshVariables)
				{
					RefreshVariables ();

					// Если Гопа покупается за просмотр видео
					if(gops [Manager.ShopGopaNum].video)
					{
						Semki_img.enabled = false;
						video_img.enabled = true;
					}
					else
					{
						Semki_img.enabled = true;
						video_img.enabled = false;
					}

					refreshVariables = false;
				}
			}
		}
	}

	public void Buy()
	{
		if(bool.Parse(PlayerPrefs.GetString ("GopaBuyed" + Manager.ShopGopaNum))) // Если персонаж куплен
		{
			// Вырубаем выбор у всех
			foreach (GopaHaracteristics GH in gops)
			{
				GH.choice = false;
			}
			// Включаем выбор у текущего Гопы
			PlayerPrefs.SetInt ("ChoicedGopa", Manager.ShopGopaNum);
			MainGopa.sprite = gops [Manager.ShopGopaNum].sprite;
			gops [Manager.ShopGopaNum].choice = true;
			buy_text.text = LanguageSystem.lng.alltexts[4];

			if(Manager.ShopGopaNum == 6)
			{
				Manager.PrazdnicAudio = true;
				manager.PrazdnikPlay ();
			}
		}
		else // Если персонаж не куплен
		{

			if(!gops [Manager.ShopGopaNum].video) // Если покупка за семешки
			{
				if(PlayerPrefs.GetInt ("Semki") >= gops [Manager.ShopGopaNum].price) // Если хватает денег на покупку
				{
					error = Panels [0].GetComponentInChildren<Text> ();
					error.text = LanguageSystem.lng.alltexts[5] + gops [Manager.ShopGopaNum].price+LanguageSystem.lng.alltexts[6];
					Panels [0].active = true;
				}
				else // Если игрок нищий
				{
					error = Panels [2].GetComponentInChildren<Text> ();
					error.text = LanguageSystem.lng.alltexts[9];
					Panels [2].active = true;
				}
			}
			else
			{
				Panels [1].active = true;
			}

		}
	}

	public void Yes() // Согласен
	{
		if(!gops [Manager.ShopGopaNum].video) // Если за семешки
		{
			if(PlayerPrefs.GetInt ("Semki") >= gops [Manager.ShopGopaNum].price) // Если хватает денег на покупку
			{
				// Обновления
				PlayerPrefs.SetInt ("Semki", PlayerPrefs.GetInt ("Semki") - gops [Manager.ShopGopaNum].price);
				semki.text = PlayerPrefs.GetInt ("Semki").ToString ();
				// Вырубаем выбор у всех
				foreach (GopaHaracteristics GH in gops)
				{
					GH.choice = false;
				}
				// Включаем выбор у текущего Гопы
				PlayerPrefs.SetString ("GopaBuyed" + Manager.ShopGopaNum, "true");
				gops [Manager.ShopGopaNum].buyed = true;
				PlayerPrefs.SetInt ("ChoicedGopa", Manager.ShopGopaNum);
				MainGopa.sprite = gops [Manager.ShopGopaNum].sprite;
				gops [Manager.ShopGopaNum].choice = true;
				buy_text.text = LanguageSystem.lng.alltexts[4];

				if(Manager.ShopGopaNum == 6)
				{
					Manager.PrazdnicAudio = true;
					manager.PrazdnikPlay ();
				}
			}
		}
		else // Если за видос
		{
			Gopa tochks_G = MainGopa.GetComponent<Gopa> ();
			Manager.adsReward = "Gopa " + Manager.ShopGopaNum;
			#if !UNITY_STANDALONE // Если не ПК версия
			tochks_G.ShowAds ("reward");
			#endif
			error = Panels [2].GetComponentInChildren<Text> ();
			error.text = LanguageSystem.lng.alltexts[17];
			Panels [2].active = true;
			// В Gopa в ShowAds само все назначится
		}

		Panels [0].active = false;
		Panels [1].active = false; // Если меня навестит муза, можно сделать foreach при большом кол-ве панелек переубеждения
	}

	public void No() // Не согласен
	{


		Panels [0].active = false;
		Panels [1].active = false;
	}

	public void Ok() // А че делать? (предупреждение об ошибке)
	{
		Panels [2].active = false;
	}

	// Ищем и обновляем переменные из PlayerPrefs
	public void ChangeVariablesFromPrefs()
	{
		gops [PlayerPrefs.GetInt ("ChoicedGopa")].choice = true;
		MainGopa.sprite = gops [PlayerPrefs.GetInt ("ChoicedGopa")].sprite; // Обновляем Гопу в игре

		for(int i = 0; i < gops.Length; ++i)
		{
			// Стеракалка PlayerPrefs.DeleteKey ("GopaBuyed"+i);
			if(PlayerPrefs.GetString ("GopaBuyed" + i) != string.Empty)
			{
				gops [i].buyed = bool.Parse (PlayerPrefs.GetString ("GopaBuyed" + i));
			}
			else
			{
				PlayerPrefs.SetString ("GopaBuyed" + i, "false");
			}
		}
	}

	// Обновляем данные интерфейса в магазине
	public void RefreshVariables()
	{
		// Присваиваем данные текстам в интерфейсе
		semki.text = PlayerPrefs.GetInt("Semki").ToString();
		price.text = gops[Manager.ShopGopaNum].price.ToString();
		k_semok.text = "x"+gops[Manager.ShopGopaNum].k_semki;
		k_health.text = "x"+gops[Manager.ShopGopaNum].k_health;

		// Меняем название кнопошки
		if(gops[Manager.ShopGopaNum].buyed && gops[Manager.ShopGopaNum].choice) // Если Гопа куплен и выбран
		{
			price_gm.active = false;
			buy_text.text = LanguageSystem.lng.alltexts[4];
		}
		else if(gops[Manager.ShopGopaNum].buyed && !gops[Manager.ShopGopaNum].choice) // Если Гопа куплен, но не выбран
		{
			price_gm.active = false;
			buy_text.text = LanguageSystem.lng.alltexts[11];
		}
		else // Если Гопа не куплен
		{
			price_gm.active = true;
			buy_text.text = LanguageSystem.lng.alltexts[12];
		}
	}

	IEnumerator TimeFromURL()
	{
		WWW date_url = new WWW ("http://xn----7sbncprpmu8c4c.xn--p1ai/time");
		yield return date_url;

		//Debug.Log (date_url.text);
		string[] date_and_time = date_url.text.Split (" " [0]);
		string[] date_data = date_and_time [0].Split ("-"[0]);
		string Date = date_data[2]+"."+date_data[1];
		//Debug.Log(date_and_time[2]);
		if(Date == date_and_time[2])
			gops [6].price = 0;

	}
}
