﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using FTRuntime;
using FTRuntime.Yields;

public class Tutor : MonoBehaviour {

	public float time;
	private float curtime, speed;
	public GameObject[] tutros, press_to_restart;
	public SwfClipController press_to_restart_swf;
	bool restart;

	void Start()
	{
		curtime = time;
		//Time.timeScale = 0;
	}
	
	void Update()
	{
		if(Time.timeScale == 0)
		{
			if(Input.touchCount > 0 || Input.GetMouseButton (0))
			{
				Time.timeScale = 1;
				if(curtime == 100)
				{
					foreach (GameObject gm in tutros)
					{
						Destroy (gm);
					}
				}
			}
		}
		else
		{
			if(speed < 1)
			{
				speed += Time.deltaTime;
				Time.timeScale = 0.5f;
			}
			else
			{
				Time.timeScale = 1;
			}

			curtime -= Time.deltaTime;
			if(curtime <= 0)
			{
				Time.timeScale = 0;
				curtime = 100;
			}
		}


		// Если мы умерли, и анимация не работает - запускаем анимашку
		if(Manager.gameOver && !press_to_restart_swf.isPlaying)
		{
			StartCoroutine (RestartBut ());
		}
		else if(!Manager.gameOver && restart) // Если мы уже запускали анимацию, а игрок начал игру сначала - удаляем лишнее
		{
			foreach (GameObject gm in press_to_restart)
				Destroy (gm);

			Destroy (gameObject); // Да удаляем ваще все, ну нафиг!
		}
	}

	IEnumerator RestartBut()
	{
		press_to_restart_swf.Play (true);
		yield return new WaitForSeconds (1);
		restart = true;
	}
}
