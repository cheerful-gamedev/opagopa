﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using UnityEngine.Advertisements; // Рекламка

public class Gopa : MonoBehaviour {

//	Float
	public float
		maxjumpForce;

	private float
		jumpForce;

//	Int
	public int
		score,
		maxscore,
		semki; // кол-во семешек

//	Text
	public Text
		score_txt, // Текст для вывода текущих очков
		maxscore_txt; // Текст для вывода максимальных очков

//	PUBLIC
	public GameObject Shop_gm, // Шмагазик
	GameObjects_gm, // Объекты интерфейса в игре
	exlusiveOffer; // предложение века (видео за 2 семки)
	public Transform restart_but; // Кнопка рестарта
	public AudioSource audioSource; // Ссыль на воспроизводитель
	public AudioClip[] clips; // Ссыль на треки
	/* 0 - шлепок по ботану
	 * 1 - бульк
	 */

//	PRIVATE
	private Rigidbody2D rgb;
	private GameManager manager;
	private RaycastHit2D hit;
	private Shop shop;
	private int k_health; // Кол-во оставшихся жизней

	// Новое управление
	private Vector2 startPos, endPos;

	#if UNITY_IOS
		private string adsID = "1600031";
	#elif UNITY_ANDROID
		private string adsID = "1600032";
	#elif UNITY_EDITOR
		private string adsID = "1600032";
	#endif

	void Start()
	{
		//Time.timeScale = 0;
		// Назначаем кол-во жизней
		k_health = shop.gops [PlayerPrefs.GetInt ("ChoicedGopa")].k_health;

		// Шмагия, как Шмаль, ток не курят
		PlayerPrefs.SetString ("GopaBuyed" + 0,"true");
		Manager.ShopGopaNum = PlayerPrefs.GetInt ("ChoicedGopa");

		// Если не ПК версия
		#if !UNITY_STANDALONE
		// Инициализируем наш AdsID, если устройство поддерживает рекламу
		if(Advertisement.isSupported)
		{
			Advertisement.Initialize (adsID, false);
		}
		else
		{
			Debug.Log ("Platform is not supported");
		}
		#endif

		Manager.showAds = false;

		// Обнуление и вывод свеженьких результатов
		if(PlayerPrefs.GetInt ("MaxScore") == null)
			PlayerPrefs.SetInt ("MaxScore", 0);
		maxscore = PlayerPrefs.GetInt ("MaxScore");
		maxscore_txt.text = LanguageSystem.lng.alltexts[1]+maxscore.ToString ();
		score = 0;
		score_txt.text = LanguageSystem.lng.alltexts[0]+score.ToString ();

		manager.PrazdnikPlay ();

		ForceMath();
	}

	// Поиск ништячков
	void Awake()
	{
		rgb = GetComponent<Rigidbody2D> ();
		manager = GameObject.Find ("Game Manager").GetComponent<GameManager> ();
		shop = GameObject.Find ("Shop").GetComponent<Shop> ();
	}

	void FixedUpdate()
	{
		// // Увеличение силы прыжка при нажатии на экран, и его обнулении при отсутствие нажатия
		// if(Input.touchCount > 0 || Input.GetMouseButton (0))
		// {
		// 		ForceMath ();
		// }
		// else
		// {
		// 	jumpForce = 0;
		// 	Manager.sunState = "idle";
		// }

		// Если не ПК версия
		#if !UNITY_STANDALONE
		if(!Advertisement.isShowing)
		{
			Manager.showAds = false;
		}
		#endif

		// Кутимся
		Fliping ();

		// Мы умерли, над что-то с этим делать       OPTIMAZE!!!
		if(Manager.gameOver)
		{
			if(Manager.shopShow)
			{
				// убираем кнопошку перезапуска и деактивируем ее
				TextAnimation (restart_but, new Vector3 (0, -12, 0), 0.05f);
				restart_but.transform.Find("Restart_but").GetComponent<Button> ().interactable = false; // По сути можно убрать!

				// Показываем шамагазик
				TextAnimation (Shop_gm.transform, new Vector2 (0, 0), 0.1f);
				Shop_gm.transform.position = new Vector3 (Shop_gm.transform.position.x, Shop_gm.transform.position.y, 10);
				Shop_gm.GetComponent<Shop>().enabled = true;
			}
			else
			{
				// убираем шамагазик
				TextAnimation (Shop_gm.transform, new Vector2 (0, 12), 0.1f); // Прятки с магазиком
				Shop_gm.transform.position = new Vector3 (Shop_gm.transform.position.x, Shop_gm.transform.position.y, 10);
				Shop_gm.GetComponent<Shop>().enabled = false;

				// Активируем кнопошку и показываем ее (или наоборот)
				TextAnimation (restart_but, new Vector3 (0, 0, 0), 0.1f);
				restart_but.transform.Find("Restart_but").GetComponent<Button> ().interactable = true; // По сути можно убрать!
			}
		}
		else
		{
			// убираем кнопошку перезапуска и деактивируем ее
			TextAnimation (restart_but, new Vector3 (0, -12, 0), 0.05f);
			restart_but.transform.Find("Restart_but").GetComponent<Button> ().interactable = false; // По сути можно убрать!

			if(!Manager.shopShow)
			{
				// убираем шамагазик
				TextAnimation (Shop_gm.transform, new Vector2 (0, 12), 0.1f); // Прятки с магазиком
				Shop_gm.transform.position = new Vector3 (Shop_gm.transform.position.x, Shop_gm.transform.position.y, 10);
				Shop_gm.GetComponent<Shop>().enabled = false;
			}
		}
	}

	// Перевороты после прыжка и корректировка при приземлении
	void Fliping()
	{
		/*/ СТАРОЕ Поиск ближайщего ботана
		int noobNum = 0;
		float mindist = 10000;
		for(int i = 0; i < manager.noobs.Count; i++)
		{
			float dis = Vector2.Distance (transform.position, manager.noobs [i].transform.position);
			if(dis < mindist)
			{
				mindist = dis;
				noobNum = i;
			}
		}*/

		// Сальтухи в воздухе и остановка поворотов на ботане
		// СТАРОЕ if(mindist <= 2 || transform.position.y <= -3.2f)
		if(transform.position.y <= -2.2f)
		{
			Slerp (0);
		}
		else
		{
			transform.RotateAround (transform.position, Vector3.forward, Time.deltaTime * 1000);
		}

		// Если мы провалились, надо вернуться в этот мир
		if(transform.position.y < -12 && !Manager.gameOver)
		{
			transform.position = new Vector2 (transform.position.x, 12);
			score_txt.text = LanguageSystem.lng.alltexts[15];
		}
	}

	// Мягочное вращение Гопы
	void Slerp(float z)
	{
		transform.rotation = Quaternion.Slerp (transform.rotation, Quaternion.Euler (0, 0, z), Time.deltaTime * 100);
	}

	// Ускорение
	void ForceMath()
	{
		// if(jumpForce < maxjumpForce)
		// 	jumpForce += 0.5f;

		// Transform nextNoob = manager.GetNextNoob();
		// float distance = Vector2.Distance(transform.position, nextNoob.position) * 1.37f;
		// Debug.Log(distance);
		jumpForce = Random.Range(3f, maxjumpForce);

		// Солнечная анимашка
		if(Manager.sunState != "surprise" && !Manager.gameOver && jumpForce >= maxjumpForce / 2)
		{
			Manager.sunState = "surprise";
			manager.ChangeSunState ();
		}
		else if(Manager.sunState == "surprise")
		{
			manager.Sun_SwfClip.currentFrame = 11;
		}
	}

	// Реализация прыжка с помощью черной магии
	void Jump(float force)
	{
		rgb.velocity = new Vector3 (0, force);
		jumpForce = 0;
		Manager.sunState = "idle";

		ForceMath();
	}

	// Мы чет тронули? Фууу!
	void OnTriggerEnter2D (Collider2D col)
	{
		GameObject tuchObj = col.gameObject; // Присваиваем объект к объекту для удобного объекто-ориентированного программирования
		if(tuchObj.tag == "Lava") // Пол - это лава!
		{
			if(k_health > 1)
			{
				LavaTuch ();
			}
			else
			{
				GameOver ();
			}
		}
		else if(!Manager.gameOver && tuchObj.tag == "Noob" && tuchObj.name != Manager.CurNoobName) // Проверка на единичность касания
		{
			NoobTuch (tuchObj);
		}
		else if(!Manager.gameOver && tuchObj.tag == "GoldNoob" && tuchObj.name != Manager.CurNoobName) // Проверка на единичность касания
		{
			NoobTuch (tuchObj);

			// + семешка
			PlayerPrefs.SetInt ("Semki", PlayerPrefs.GetInt ("Semki") + shop.gops [PlayerPrefs.GetInt ("ChoicedGopa")].k_semki);
			manager.Semka_SwfClipController.Play (true); // Семешка
		}
	}

	// Действия при косании нубов (ну чтоб по семь раз не копировать код)
	void NoobTuch(GameObject tuchObj)
	{
		Noob gm_noob = tuchObj.GetComponent<Noob> ();
		audioSource.PlayOneShot (clips [0]); // Звук шлепко по попке ботана

		//if(Input.touchCount == 0 || Input.GetMouseButtonUp (0)) // Тут мы понизили сложность игры для лошков, что плакались мне
		Jump (jumpForce);

		Manager.CurNoobName = tuchObj.name; // Присваиваем имя пострадавшего для проверки на единичность касания (выше на несколько строчек)

		gm_noob.Die (); // Запускаем смерть ботана
		//manager.SpawnNoob (); // Спавним нового

		// + к карме
		score++;
		score_txt.text = LanguageSystem.lng.alltexts[0] + score.ToString ();
	}

	// Действия при косании лавы (если жизней мнохо)
	void LavaTuch()
	{
		audioSource.PlayOneShot (clips [0]); // Звук шлепко по попке ботана

		//manager.SpawnNoob (); // Спавним нового

		//if(Input.touchCount == 0 || Input.GetMouseButtonUp (0)) // Тут мы понизили сложность игры для лошков, что плакались мне
		if(jumpForce == 0)
		Jump (5);
		else
		Jump(jumpForce);

		k_health--;
	}

	// Проиграли
	void GameOver()
	{
		rgb.velocity = new Vector2 (0,0); // Обнуляем скорость, чтобы если Гопа успел каким-то чудом коснуться ботана - остановился и помер

		StartCoroutine(ShopAnim()); // Магазик

		if(score > PlayerPrefs.GetInt ("MaxScore")) // Обновляем рекорд
			PlayerPrefs.SetInt ("MaxScore", score);

		Manager.gameOver = true;

		// Сколько раз умерли (больше 20 - показ рекламы)
		int Dies = PlayerPrefs.GetInt ("ShowAds");
		Dies++;
		if(Dies >= 30) // Если кол-во смертей достигло 30
		{
			Dies = 0;
			PlayerPrefs.SetInt ("ShowAds", Dies);
			exlusiveOffer.active = true;

			/*// Если не ПК версия
			#if !UNITY_STANDALONE
			Manager.adsReward = "Die";
			ShowAds ("die");
			#endif*/ //Убрал рекламуку Т-Т хнык-хнык
		}
		else if(Dies == 10 || Dies == 20) // Если кол-во смертей достигло 10
		{
			PlayerPrefs.SetInt ("ShowAds", Dies);
			exlusiveOffer.active = true;
		}
		else
		{
			PlayerPrefs.SetInt ("ShowAds", Dies);
		}

		audioSource.PlayOneShot (clips [1]); // Звук булька
		manager.Bulk_SwfClipController.Play (true); // Бульканимашка

		// Солнечная анимашка
		Manager.sunState = "happy";
		manager.ChangeSunState ();
	}

	//Если не ПК версия
	#if !UNITY_STANDALONE
	// Денюжка ( $_$)
	public void ShowAds(string videoID)
	{
		ShowOptions options = new ShowOptions();
		options.resultCallback = AdCallbackhanler;
		if(Advertisement.IsReady ())
		{
			Manager.showAds = true;
			Advertisement.Show (videoID, options);
		}
		else
		{
			Text shp_txt = shop.Panels [2].GetComponentInChildren<Text> ();
			shp_txt.text = LanguageSystem.lng.alltexts[16];
			shop.Panels [2].active = true;
		}
	}

	void AdCallbackhanler(ShowResult result){
		switch (result){
			case ShowResult.Finished: // Видео закончилось
				// "Gopa 4" - получаем гопу 4 по завершению, "Die" - ничего не получаем, "Semki" + 2 семки
		shop.Panels [2].active = false; // Прячим окно, что видео грузится (если оно появлялось)
				switch (Manager.adsReward)
				{
					// Видео за то что сдох (лошара)
					case "Die":
						// Ниче не делаем, все чики-пуки
						break;

					// Видео за семки
					case "Semki":
						PlayerPrefs.SetInt ("Semki", PlayerPrefs.GetInt ("Semki") + 2);
						break;

					//	Видео за Гопу
					default:
						string[] gpNum_txt = Manager.adsReward.Split (" " [0]);
						shop.gops [int.Parse (gpNum_txt [1])].price--;
						// Если тип осталось 0 просмотров (нууу все уже глянул, красава, герой моего и Дименого кошелька)
						if(shop.gops [int.Parse (gpNum_txt [1])].price <= 0)
						{
							// Вырубаем выбор у всех
							foreach (Shop.GopaHaracteristics GH in shop.gops)
							{
								GH.choice = false;
							}
							// Включаем выбор у текущего Гопы
							PlayerPrefs.SetString ("GopaBuyed" + gpNum_txt [1], "true");
							// Если этот персонаж все так куплен, а то мало ли ( -_-)
							PlayerPrefs.SetInt ("ChoicedGopa", int.Parse (gpNum_txt [1]));
							shop.ChangeVariablesFromPrefs ();
							//shop.RefreshVariables ();
						}
						shop.RefreshVariables ();
						break;
				}
				break;

			// Игрок скипнул
			case ShowResult.Skipped:
				Debug.Log("Skipped");
				break;

			// Ошибка
			case ShowResult.Failed:
				Text shp_txt = shop.Panels [2].GetComponentInChildren<Text> ();
				shp_txt.text = LanguageSystem.lng.alltexts[16];
				shop.Panels [2].active = true;
				break;
		}
	}
	#endif

	// Перезапуск с обнулением переменных
	public void Restart()
	{
		if(!Manager.showAds)
		{
			Back ();

			k_health = shop.gops [PlayerPrefs.GetInt ("ChoicedGopa")].k_health;
			// Обнуляем глобальные переменные
			Manager.sunState = "idle";
			manager.ChangeSunState ();
			Manager.CurNoobName = "";
			Manager.gameOver = false;
			Manager.shopShow = false;
			Manager.speed = 3;
			manager.curNoob = 1;

			//Application.LoadLevel (Application.loadedLevel);

			// Обновляем и обнуляем результаты игровой деятельности
			if(PlayerPrefs.GetInt ("MaxScore") == null)
				PlayerPrefs.SetInt ("MaxScore", 0);
			maxscore = PlayerPrefs.GetInt ("MaxScore");
			maxscore_txt.text = LanguageSystem.lng.alltexts[1] + maxscore.ToString ();
			score = 0;
			score_txt.text = LanguageSystem.lng.alltexts[0] + score.ToString ();

			// Возвращаем ботанов на исходную
			List<GameObject> gm = new List<GameObject> (manager.noobs);
			for(int i = 0; i < gm.Count; ++i)
			{
				if(i == 0)
					gm [i].transform.position = new Vector2 (2f, -5f);
				else
					gm [i].transform.position = new Vector2 (50, -5f);

				Noob gm_noob = gm [i].GetComponent<Noob> ();
				gm [i].GetComponent<Rigidbody2D> ().velocity = Vector3.zero;
				gm_noob.Start ();
			}
			manager.Start ();

			// Гопа, вернись на место, плиз
			this.transform.position = new Vector2 (0, 2);
			rgb.velocity = Vector3.zero;
		}
	}

	// Показываем предложение века (видео за две семки)
	public void ShowExclusiveOffer()
	{
		Manager.adsReward = "Semki";
		#if !UNITY_STANDALONE
		ShowAds ("reward");
		#endif
		exlusiveOffer.active = false;
	}

	// Анимация появления текстулички (ну или кнопошек)
	void TextAnimation(Transform text_obj, Vector3 posTo, float timeTo)
	{
		text_obj.position = Vector3.Lerp (text_obj.position, posTo, timeTo);
	}

	// Шмальгазик :3
	public void Shop()
	{
		GameObjects_gm.active = false;
		StartCoroutine(ShopAnim()); // Магазик
		shop.RefreshVariables();
		Manager.shopShow = true;
	}

	IEnumerator ShopAnim()
	{
		yield return new WaitForSeconds (.2f);
		manager.Shop_SwfClipController.Play (true);
	}

	public void Back()
	{
		GameObjects_gm.active = true;
		manager.Shop_SwfClipController.Play (true); // Магазик
		Manager.shopShow = false;
	}
}
