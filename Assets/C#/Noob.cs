﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//using FTRuntime;
//using FTRuntime.Yields;

public class Noob : MonoBehaviour {

//	PRIVATE
	private Rigidbody2D rgb; // меее
	private bool started;

	public void Start()
	{
		started = true;
		// rgb.AddForce(-Vector2.right*Manager.speed*150);
	}

	// public void SpeedUp()
	// {
	// 	rgb.AddForce(-Vector2.right*Manager.speed*10); // Прибавляем по чуть-чуть
	// 	Manager.speed++;
	// }

	void FixedUpdate()
	{
		// Debug.Log("Speed: "+Manager.speed);

		if(!started) return;
		transform.Translate(Vector2.left * Manager.speed * Time.deltaTime);
	}

	void Awake()
	{
		rgb = GetComponent<Rigidbody2D>();
	}

	// Потопили ботана
	public void Die()
	{
		StartCoroutine (Sinking ()); // И запускаем новую
	}

	// Тонем
	IEnumerator Sinking()
	{
		StartCoroutine (Swiming (5));
		yield return new WaitForSeconds (1f*(1-(rgb.velocity.x-1)/Manager.maxSpeed)); // Ожидание зависит от скорости ботаника
		StartCoroutine (Swiming (3.2f));
	}

	//Анимация погружения и всплытия
	public IEnumerator Swiming(float pos_y)
	{
		float max_i = 10;
		float startPos = transform.position.y;
		
		for(float i = 0; i < max_i; ++i)
		{
			transform.position = new Vector2 (transform.position.x, (startPos*(1-i/max_i))-(pos_y*(i/max_i))); // Плавная анимация погружения
			yield return new WaitForSeconds (0f);
		}
	}
}
