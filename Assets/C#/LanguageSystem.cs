using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using UnityEngine.UI;

using FTRuntime;
using FTRuntime.Yields;

public class LanguageSystem : MonoBehaviour {

//	PULBIC
	public Image flag_img; // Флаг, что на экране
	public Sprite[] flags; // Sprit'ы флагов стран

	public AllTexts[] texts;

	[System.Serializable]
	public class AllTexts // Класс ;)
	{
		public Text[] text;
	}

	public SwfClipAsset[] restart_slips;
	public SwfClip press_to_restart;

//	PRIVATE
	[HideInInspector]
	public string json;
	public static lang lng = new lang();
	private string[] langs = {"ru_RU", "en_US"}; // Названия языкав
	private int langIndex; // Номер текущего языка

	void Start() {}

	void Awake()
	{
		// В зависимости от языка устройства выбираем язык игры
		if(Application.systemLanguage == SystemLanguage.Russian || Application.systemLanguage == SystemLanguage.Ukrainian || Application.systemLanguage == SystemLanguage.Belarusian)
			PlayerPrefs.SetString("Language", "ru_RU");
		else
			PlayerPrefs.SetString("Language", "en_US");

		LangLoad(); // Подгружаем язык
		ChangeAllText(); // Заменяем текста
	}

	void LangLoad()
	{
		string filePath = System.IO.Path.Combine(
			Application.streamingAssetsPath,
			"Languages/" + PlayerPrefs.GetString("Language") + ".json"
		);

		if (filePath.Contains("://"))
		{
			WWW www = new WWW(filePath);
			while(!www.isDone) {}
			json = www.text;
		}
		else
			json = System.IO.File.ReadAllText(filePath);

		lng = JsonUtility.FromJson<lang>(json);
	}

	public class lang // Класс ;)
	{
		public string[] alltexts;

		// public string
		// 	curgops, // 0
		// 	maxgops, // 1
		// 	restart, // 2
		// 	back, // 3
		// 	selected, // 4
		// 	want_to_buy, // 5
		// 	want_to_buy2, // 6
		// 	can_you_not_see,// 7
		// 	due_to_the_fact, // 8
		// 	nishiy, // 9
		// 	yasno, // 10
		// 	choose, // 11
		// 	buy, // 12
		// 	no, // 13
		// 	yes, // 14
		// buy_text, // 15
		// video_error, // 16
		// video_is_loading, // 17
		// tutor, // 18
		// real_restart; // 19
	}

	// Сменя языка на следующий
	public void SwitchLang()
	{
		// След. флаг
		if (langIndex != langs.Length - 1)
			++langIndex;
		else
			langIndex = 0;

		// Замена данных о языке и смена картинки флага
		PlayerPrefs.SetString("Language", langs[langIndex]);
		flag_img.sprite = flags [langIndex];

		LangLoad();
		ChangeAllText();
	}

	// Перевод всех текстов на текущий язык
	void ChangeAllText()
	{
		for(int j = 0, i = 0; j < texts.Length; ++j)
			for(i = 0; i < texts[j].text.Length; ++i)
				texts[j].text[i].text = lng.alltexts[j];


		// Замена SWF клипа, для стрелочки указывающей на перезапуск игры
		if(PlayerPrefs.GetString ("Language") == "ru_RU")
			press_to_restart.clip = restart_slips [0];
		else
			press_to_restart.clip = restart_slips [1];
	}
}
